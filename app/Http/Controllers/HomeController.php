<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Classe;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function admin()
    {
        $users = User::select('id', 'name as username', 'admin_access')
                    ->get();

        return view('admin', compact('users'));
    }

    public function listClasses(Request $request)
    {
        $response_array["status"] = "success";
        $response_array["message"] = "";

        $classes = Classe::select('acronym', 'fullName')
                        ->get();

        if($classes) {
            $response_array["message"] = "Liste des utilisateurs récupérée avec succès";
            $response_array["return"]["classes"] = $classes->toArray();
        } else {
            $response_array["message"] = "Aucune classe trouvé";
        }

        return $response_array;
    }

    public function addPoints(Request $request)
    {
        $request->validate([
            'classe' => 'required|string',
            'points' => 'required|numeric'
        ]);

        $response_array["status"] = "success";
        $response_array["message"] = "";

        $classe = Classe::where('acronym', $request->classe)
                        ->get()->first();

        if($classe) {
            $classe->points += $request->points;

            $classe->save();
        } else {
            $response_array["status"] = "error";
            $response_array["message"] = "Une erreur est survenue lors de l'ajout des points";
        }

        return $response_array;
    }

    public function listUsers(Request $request)
    {
        $response_array["status"] = "success";
        $response_array["message"] = "";

        $users = User::select('id', 'name as username', 'admin_access')
                        ->get();

        if($users) {
            $response_array["message"] = "Liste des utilisateurs récupérée avec succès";
            $response_array["return"]["users"] = $users->toArray();
        } else {
            $response_array["message"] = "Aucun utilisateur trouvé";
        }

        return $response_array;
    }

    public function addUser(Request $request)
    {
        $request->validate([
            'username' => 'required|string',
            'password' => 'required|string',
            'admin_access' => 'required|boolean'
        ]);

        $response_array["status"] = "success";
        $response_array["message"] = "";

        if(!User::where('name', $request->username)) {

            $user = new User();
            $user->email = $request->username . '@institut-lemonnier.fr';
            $user->password = Hash::make($request->password);
            $user->name = $request->username;
            $user->admin_access = $request->admin_access;

            $user->save();

            if($user->save()) {
                $response_array["message"] = "utilisateur ajouté avec succès";
                $response_array["return"]["added_user"] = $user->id;
            } else {
                $response_array["status"] = "error";
                $response_array["message"] = "Une erreur est survenue lors de l'ajout de l'utilisateur (TROP LONG ??? c'est ça de s'endormir sur son clavier ou de laisser un singe se charger de l'administration)";
            }

            return $response_array;
        }

        $response_array["status"] = "error";
        $response_array["message"] = "Un utilisateur portant ce nom existe déjà";

        return $response_array;
    }

    public function deleteUser(Request $request)
    {
        $request->validate([
            'user_ID' => 'required|numeric|exists:mysql.users,id'
        ]);

        $response_array["status"] = "success";
        $response_array["message"] = "";

        if(User::where('id', $request->user_ID)->delete())
        {
            $response_array["message"] = "utilisateur supprimé avec succès";
        }
        else
        {
            $response_array["status"] = "error";
            $response_array["message"] = "Un problème est survenu lors de la suppression de l'utilisateur";
        }

        return $response_array;
    }

    public function updateUserName(Request $request)
    {
        $request->validate([
            'user_ID' => 'required|numeric|exists:mysql.users,id',
            'new_username' => 'required|string'
        ]);

        $response_array["status"] = "success";
        $response_array["message"] = "";

        $user = User::where('id', $request->user_ID);

        if(User::where('id', $request->user_ID)) {
            $user->email = $request->new_username . '@institut-lemonnier.fr';
            $user->name = $request->new_username;

            if($user->save()) {
                $response_array["message"] = "Le nom d'utilisateur a été modifié avec succès";
            }
            else
            {
                $response_array["status"] = "error";
                $response_array["message"] = "Un problème est survenu lors de la suppression de l'utilisateur";
            }
        } else {
            $response_array["status"] = "error";
            $response_array["message"] = "L'utilisateur est introuvable";
        }

        return $response_array;
    }

    public function updateAdminRight(Request $request)
    {
        $request->validate([
            'user_ID' => 'required|numeric|exists:mysql.users,id',
            'admin_access' => 'required|boolean'
        ]);

        $response_array["status"] = "success";
        $response_array["message"] = "";

        $user = User::where('id', $request->user_ID);

        if(User::where('id', $request->user_ID)) {
            $user->admin_access = $request->admin_access;

            if($user->save()) {
                $response_array["message"] = "Le droit d'administration a bien été mofidié";
            }
            else
            {
                $response_array["status"] = "error";
                $response_array["message"] = "Un problème est survenu lors de la suppression de l'utilisateur";
            }
        } else {
            $response_array["status"] = "error";
            $response_array["message"] = "L'utilisateur est introuvable";
        }

        return $response_array;
    }

    public function saveScorePageConfig(Request $request)
    {
        Storage::disk('config')->put('score_config.json', $request->config);
    }
}
