<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Classe;
use Illuminate\Support\Facades\Storage;

class PageController extends Controller
{
    public function score(Request $request) {
        return view('score');
    }

    public function radio(Request $request) {
        return view('radio');
    }

    public function getScoreConfig(Request $request) {
        return Storage::disk('config')->get('score_config.json');
    }

    public function getTopScore(Request $request) {
        $response_array["status"] = "success";
        $response_array["message"] = "";

        if(!empty($request->numberOfResults) && ctype_digit($request->numberOfResults) && intval($request->numberOfResults) > 0)
        {
            $scoreList = Classe::selectRaw("acronym, ROUND(points / effective) * " . Classe::max('effective') . " AS 'points'")
                                ->orderByRaw('points DESC')
                                ->paginate($request->numberOfResults);

            if ($scoreList)
            {
                $response_array["message"] = "Liste des scores récupérée avec succès";
                $response_array["return"]["scoreList"] = $scoreList->toArray()['data'];
            }
            else
            {
                $response_array["status"] = "error";
                $response_array["message"] = "Une erreur est survenue lors de la récupération des scores";
            }
        }
        else
        {
            $response_array["status"] = "error";
            $response_array["message"] = "Veuillez entrer un nombre d'entrées à renvoyer correct (supérieur à 0)";
        }

        return $response_array;
    }
}
