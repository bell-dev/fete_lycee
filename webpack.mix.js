const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css')

   .js('resources/js/score.js', 'public/js')
   .sass('resources/sass/score.scss', 'public/css')
   .js('resources/js/radio.js', 'public/js')
   .sass('resources/sass/radio.scss', 'public/css')
   .js('resources/js/admin.js', 'public/js')
   .sass('resources/sass/admin.scss', 'public/css')

   .js('resources/js/points.js', 'public/js')

   .sass('resources/sass/home.scss', 'public/css')

   .copyDirectory('resources/img', 'public/img')
   .copyDirectory('resources/config', 'public/config')
   .sourceMaps();
