<?php

use Illuminate\Database\Seeder;

class ClassesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('classes')->insert([
            [
                'acronym' => '2NDE A',
                'fullName' => 'SECONDE TECHNOLOGIQUE A',
                'effective' => 31
            ],
            [
                'acronym' => '1TS TC',
                'fullName' => '1ERE ANNEE TECH COMMERCE',
                'effective' => 20
            ],
            [
                'acronym' => '2TS TC',
                'fullName' => '2EME ANNEE TECH COMMERCE',
                'effective' => 20
            ],
            [
                'acronym' => '2NDE B',
                'fullName' => 'SECONDE TECHNOLOGIQUE B',
                'effective' => 32
            ],
            [
                'acronym' => '1 S',
                'fullName' => '1ERE S SCIENCES INGENIEUR',
                'effective' => 14
            ],
            [
                'acronym' => '1 STI2D B',
                'fullName' => '1 STI 2D SIN - EE',
                'effective' => 30
            ],
            [
                'acronym' => '1 STI2D A',
                'fullName' => '1 STI 2D SIN - EE',
                'effective' => 28
            ],
            [
                'acronym' => 'TS',
                'fullName' => 'TERMINALE  S SCIENCES ING',
                'effective' => 14
            ],
            [
                'acronym' => 'T STI2D A',
                'fullName' => 'T STI2D SIN - EE',
                'effective' => 29
            ],
            [
                'acronym' => 'T STI2D B',
                'fullName' => 'T STI2D SIN - EE',
                'effective' => 29
            ],
            [
                'acronym' => '1TS SNIR',
                'fullName' => '1A BTS SYST NUM INF RESEA',
                'effective' => 27
            ],
            [
                'acronym' => '2TS SNIR',
                'fullName' => '2A BTS SYST NUM INF RESEA',
                'effective' => 19
            ],
            [
                'acronym' => '1TS SA',
                'fullName' => '1A BTS CONC SYST AUTOMATI',
                'effective' => 26
            ],
            [
                'acronym' => '2TS MS',
                'fullName' => '2A BTS MAINTENANCE SYSTEM',
                'effective' => 13
            ],
            [
                'acronym' => '2TS FE',
                'fullName' => '2A BTS FLUIDE ET ENERGIE',
                'effective' => 13
            ],
            [
                'acronym' => '1TS MS',
                'fullName' => '1A BTS MAINTENANCE SYSTEM',
                'effective' => 21
            ],
            [
                'acronym' => '1TS ET',
                'fullName' => '1A BTS ELECTROTECHNIQUE',
                'effective' => 13
            ],
            [
                'acronym' => '2TS ET',
                'fullName' => '2A BTS ELECTROTECHNIQUE',
                'effective' => 10
            ],
            [
                'acronym' => '1TS FE',
                'fullName' => '1A BTS FLUIDE ET ENERGIE',
                'effective' => 13
            ],
            [
                'acronym' => '2TS MS ALT',
                'fullName' => '2A BTS MAINTENANCE SYSTEM ALT',
                'effective' => 9
            ],
            [
                'acronym' => '4EAA',
                'fullName' => '4EME E. AGRICOLE',
                'effective' => 19
            ],
            [
                'acronym' => '4EAB',
                'fullName' => '4EME E. AGRICOLE',
                'effective' => 19
            ],
            [
                'acronym' => '3EAA',
                'fullName' => '3EME E. AGRICOLE',
                'effective' => 17
            ],
            [
                'acronym' => '3EAB',
                'fullName' => '3EME E. AGRICOLE',
                'effective' => 15
            ],
            [
                'acronym' => '2 CAPJ',
                'fullName' => '2EME ANNEE CAPA JARDINIER PAYSAGISTE',
                'effective' => 22
            ],
            [
                'acronym' => '2 PCV',
                'fullName' => '2NDE PRO CONSEIL VTE',
                'effective' => 16
            ],
            [
                'acronym' => '2 NJPF',
                'fullName' => '2NDE NATURE JARDIN PAYSAGE FORET',
                'effective' => 23
            ],
            [
                'acronym' => '1 CAPJ',
                'fullName' => '1ère CAPA JARDINIER PAYSAGISTE',
                'effective' => 19
            ],
            [
                'acronym' => '1 PAP',
                'fullName' => '1ERE PRO AMENAG PAYSAGERS',
                'effective' => 22
            ],
            [
                'acronym' => '1 PLCQ',
                'fullName' => '1ERE PRO LABO CONTROLE QUALITE',
                'effective' => 18
            ],
            [
                'acronym' => 'T PAP',
                'fullName' => 'TERM PRO AMENAGT PAYSAGER',
                'effective' => 23
            ],
            [
                'acronym' => 'T PTCV',
                'fullName' => 'TERM PRO TECH CONSEIL VTE',
                'effective' => 13
            ],
            [
                'acronym' => 'SAPVER1',
                'fullName' => 'SERVICE AUX PERSONNES ET VENTE EN ESPACE RURAL 1ERE ANNEE',
                'effective' => 13  
            ],
            [
                'acronym' => 'SAPVER2',
                'fullName' => 'SERVICE AUX PERSONNES ET VENTE EN ESPACE RURAL 2EME ANNEE',
                'effective' => 12
            ],
            [
                'acronym' => '1 PTMA',
                'fullName' => '1ERE PRO TMA',
                'effective' => 20
            ],
            [
                'acronym' => '1 CAPH',
                'fullName' => '1ERE CAP HORTICOLE',
                'effective' => 19
            ],
            [
                'acronym' => '2 CAPH',
                'fullName' => '2EME CAP HORTICOLE',
                'effective' => 5
            ],
            [
                'acronym' => '1 PPH',
                'fullName' => '1ERE PRO HORTICOLE',
                'effective' => 7
            ],
            [
                'acronym' => '2 PPH',
                'fullName' => '2EME PRO HORTICOLE',
                'effective' => 5
            ],
            [
                'acronym' => 'T PPH',
                'fullName' => 'TERM PRO HORTICOLE',
                'effective' => 7
            ],
            [
                'acronym' => 'CAR1',
                'fullName' => '1A CAP CARROSSERIE',
                'effective' => 20
            ],
            [
                'acronym' => '2 PMV',
                'fullName' => '2NDE PRO MV',
                'effective' => 31
            ],
            [
                'acronym' => '2 PMELEC1',
                'fullName' => '2NDE PRO MELEC1',
                'effective' => 24
            ],
            [
                'acronym' => 'CMA1',
                'fullName' => '1A CAP MENUISIER',
                'effective' => 25
            ],
            [
                'acronym' => 'CMA2',
                'fullName' => '2A CAP MENUISIER',
                'effective' => 17
            ],
            [
                'acronym' => 'MMEV2',
                'fullName' => '2A CAP MMEV',
                'effective' => 13
            ],
            [
                'acronym' => '1 BMA',
                'fullName' => '1ERE BMA EBENISTERIE',
                'effective' => 11
            ],
            [
                'acronym' => 'MMEV1',
                'fullName' => '1A CAP MMEV',
                'effective' => 11
            ],
            [
                'acronym' => '1 PMELEC',
                'fullName' => '1ERE PRO MELEC',
                'effective' => 26
            ],
            [
                'acronym' => 'TPMELEC',
                'fullName' => 'TER PRO MELEC',
                'effective' => 24
            ],
            [
                'acronym' => '3 PP1',
                'fullName' => '3EME PREPA PRO 1',
                'effective' => 24
            ],
            [
                'acronym' => '3 PP3',
                'fullName' => '3EME PREPA PRO 3',
                'effective' => 22
            ],
            [
                'acronym' => 'TPMV',
                'fullName' => 'TERM PRO MV',
                'effective' => 16
            ]
        ]);
    }
}
