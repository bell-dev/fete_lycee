<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        $users = [
            [
                'email' => 'admin@institut-lemonnier.fr',
                'password' => Hash::make('Anubis'),
                'name' => 'admin',
                'admin_access' => true
            ],
            [
                'email' => 'user1@institut-lemonnier.fr',
                'password' => Hash::make('Bastet'),
                'name' => 'user1',
                'admin_access' => false
            ],
            [
                'email' => 'user2@institut-lemonnier.fr',
                'password' => Hash::make('Chentyt'),
                'name' => 'user2',
                'admin_access' => false
            ],
            [
                'email' => 'user3@institut-lemonnier.fr',
                'password' => Hash::make('Dounanoui'),
                'name' => 'user3',
                'admin_access' => false
            ],
            [
                'email' => 'user4@institut-lemonnier.fr',
                'password' => Hash::make('Ermouthis'),
                'name' => 'user4',
                'admin_access' => false
            ],
            [
                'email' => 'user5@institut-lemonnier.fr',
                'password' => Hash::make('Fetket'),
                'name' => 'user5',
                'admin_access' => false
            ]
        ];

        foreach($users as $user) {
            User::create($user);
        }
    }
}
