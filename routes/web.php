<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/score', 'PageController@score');
Route::get('/radio', 'PageController@radio');
Route::post('GetScoreConfig', 'PageController@getScoreConfig');
Route::post('GetTopScore', 'PageController@getTopScore');

/**
 * Routes nécessitant une authentification
 */
// Auth::routes();
// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'is-admin'], function () {
    Route::get('/admin', 'HomeController@admin')->name('admin');

    Route::post('ListUsers', 'HomeController@listUsers');
    Route::post('AddUser', 'HomeController@addUser');
    Route::post('DeleteUser', 'HomeController@deleteUser');
    Route::post('UpdateUsername', 'HomeController@updateUsername');
    Route::post('UpdateAdminRight', 'HomeController@updateAdminRight');

    Route::post('SaveScorePageConfig', 'HomeController@saveScorePageConfig');
});

Route::post('ListClasses', 'HomeController@listClasses');
Route::post('AddPoints', 'HomeController@addPoints');
