<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Score Panel</title>

        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <link rel="stylesheet" href="{{ asset('css/score.css') }}">
    </head>

    <body>
        <label id="Title">Titre</label>
        <label id="Timer">Timer</label>
        <label id="Score1" class="score">Score 1</label>
        <label id="Score2" class="score">Score 2</label>
        <label id="Score3" class="score">Score 3</label>
        <label id="Score4" class="score">Score 4</label>
        <label id="Score5" class="score">Score 5</label>
        <label id="Score6" class="score">Score 6</label>
        <label id="Score7" class="score">Score 7</label>
        <label id="Score8" class="score">Score 8</label>
        <label id="Score9" class="score">Score 9</label>
        <label id="Score10" class="score">Score 10</label>
        <img id="live-gif" src="{{ asset('img/live.gif') }}">

        <div id="dialog" title="Config options">
            <input id="titleChangeText" type="text" placeholder="Entrez ici le texte du  titre">
            <br>
            <p>Durée du timer (en secondes min=10 | max=120)</p>
            <input type="number" id="timer_fresh_rate" min="10" max="120">
            <input type="button" value="Prévisualiser le timer" disabled>
            <br><br>
            <select class="form-control bfh-fonts" data-font="Arial"></select>
            <br><br>
            <p>Sélectionnez un fond d'écran personnalisé</p>
            <input type="file" value="Changer le fond d'écran" disabled>
            <br><br>
            <input type="button" value="Reset" disabled>
            <input id="saveScoreButton" type="button" value="Sauvegarder">
            <br>
            <input type="text" id="pointerLocation" readonly>
        </div>

        <script defer src="{{ asset('js/app.js') }}"></script>
        <script defer src="{{ asset('js/score.js') }}"></script>
    </body>
</html>
