@extends('layouts.app')

@section('content')
{{-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div> --}}

    <script defer src="{{ asset('js/points.js') }}"></script>

    <div class="container-element">
        <select id="classes" class="chosen control-element" style="width:500px;">
        </select>

        <br><br>

        <input type="number" class="form-control control-element" id="points" value="0">

        <br>

        <input type="button" id="buttonAddPoints" class="btn btn-info control-element" value="insérer">

        <br><br>

        <textarea id="log-console" placeholder="Aucun log à affichier pour le moment"></textarea>
    </div>
@endsection
