<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Score Panel</title>

        {{-- <script src="static/js/jquery-3.3.1/jquery.min.js"></script>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> --}}

        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    </head>

    <body>
        <table id="score_table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>classe</th>
                    <th>score</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>0</td>
                    <td>undefined</td>
                    <td>null</td>
                </tr>
            </tbody>
        </table>

        <br><br>

        <select id="number_to_show">
            <option value="5">5</option>
            <option value="10">10</option>
            <option value="15">15</option>
            <option value="20">20</option>
            <option value="100">100</option>
        </select>

        <input type="button" value="Afficher" id="updateScoreButton">

        <script defer src="{{ asset('js/app.js') }}"></script>
        <script defer src="{{ asset('js/radio.js') }}"></script>
    </body>
</html>
