<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
        <title>Gestion Admin</title>
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <link rel="stylesheet" href="{{ asset('css/admin.css') }}">
	</head>

	<body>
        <fieldset id="" hidden>
            <legend>Gestion de l'affichage des points</legend>
            <br>
            <input type="checkbox" id="checkbox_show_timer"><label for="checkbox_show_timer">Afficher le timer</label>
            <br>
            <input type="number" id="timer_fresh_rate" min="10" max="120">
            <br>
            <select id="timer_position">
                <option value="top-left">top-left</option>
                <option value="top-right">top-right</option>
                <option value="bottom-left">bottom-left</option>
                <option value="bottom-right">bottom-right</option>
            </select>
            <br>
            <input type="button" value="Enregistrer" onClick="saveConfigFile(this)">

            <br><br>
        </fieldset>

        <div id="admin-options">
            <fieldset id="">
                <br>
                <legend>Gestion des comptes</legend>
                <table border='0' cellspacing='0' cellpadding='0' id="table_users">
                    <tr>
                        <th>Utilisateur</th>
                        <th>Administrateur</th>
                        <th>Action</th>
                    </tr>

                    @foreach ($users as $user)
                        <tr id="username{{$user->id}}">
                            <td id="textzone_username_{{$user->id}}">{{$user->username}}</td>
                            <td><input type='checkbox' {{$user->admin_access ? "checked" : ""}} id='checkbox_admin_". $row["id"] ."' disabled></td>
                            <td><i class='fas fa-trash-alt fa-lg delete_button' onClick="deleteUser({{$user->id}})" title="Supprimer l'utilisateur"></i><i class='fa fa-ban ban_button' aria-hidden='true' title='BAN'></i><i class='fas fa-pencil-alt edit_button' aria-hidden='true' onClick='editUser({{$user->id}})' title='Éditer'></i></td>
                        </tr>
                    @endforeach

                    <tr class="spaceUnder">
                        <td><input type="textarea" placeholder="Nom d'Utilisateur" id="username"><input type="password" placeholder="Mot de passe" id="password"></td>
                        <td><input type='checkbox' onClick="prevent_admin_rights(this)" id="checkbox_admin"></td>
                        <td><i class='fa fa-plus fa-lg edit_button' id="addUserButton" title="Ajouter l'utilisateur"></i></td>
                    </tr>
                </table>
                <br>

                <div id="editUser_Panel" hidden>
                        <input type="textarea" placeholder="Nom d'Utilisateur" id="textbox_modify_username">
                        <input type="password" placeholder="Nouveau Mot de Passe" id="textbox_modify_password">
                        <input type="checkbox" onClick="prevent_admin_rights(this)" id="checkbox_modify_user_admin">&nbsp<label for="checkbox_modify_user_admin">Autoriser l'accès admnistrateur</label></input>
                        <br><br>
                        <input type="button" value="Annuler" class="Cancel_Button" onClick="cancelModif()">
                        <input type="button" value="Appliquer les changements" class="Validate_Button" onClick="applyModif(this)">
                </div>
                <br>
            </fieldset>

            <fieldset>
                <input id="btn_generateBDD" type="button" value="Générer la base de données" disable>
                <input type="button" id="editScorePageButton" value="Configurer la page d'affichage">
            </fieldset>

            <br>
            <ul>
                Quelques règles de base à respecter pour le bien être de tous :
                <li>Il est conseillé d'éviter les caractères spéciaux pour le nom d'utilisateur</li>
                <li>Ne pas rentrer un nom d'utilisateur trop long</li>
            </ul>
        </div>

        <script defer src="{{ asset('js/app.js') }}"></script>
        <script defer src="{{ asset('js/admin.js') }}"></script>
    </body>
</html>
