initialize();

document.getElementById('titleChangeText').addEventListener('keyup', function() {
    updateTitleText(this);
});

document.getElementById('saveScoreButton').addEventListener('click', function() {
    save();
});

document.getElementById("timer_fresh_rate").onblur = function() {
    if (parseInt(this.value) > parseInt(this.max)) {
        this.value = parseInt(this.max);
    } else if (parseInt(this.value) < parseInt(this.min)) {
        this.value = parseInt(this.min);
    }
};

function updateTitleText(element) {
    if (element.value === "") {
        document.getElementById("Title").innerHTML = "Titre";
    } else {
        document.getElementById("Title").innerHTML = element.value;
    }
}

var config_file;
var listLength = 0;
function initialize() {
    $.ajax({
        type: "POST",
        url: "GetScoreConfig",
        cache: false,
        dataType: "json",
        success: function(data) {
            config_file = data;

            document.body.style.backgroundColor =
                config_file.Points_Panel.background_color;

            document.body.style.backgroundImage =
                "url('" + config_file.Points_Panel.background_location + "')";

            if (findGetParameter("edit") === "true") {
                alert(
                    "Le mode édition est activé\nVous devez passer en mode affichage afin de pouvoir afficher les scores"
                );

                $("#dialog").dialog({
                    resizable: false,
                    closeOnEscape: false,
                    position: {
                        my: "right middle",
                        at: "right middle",
                        of: window
                    },
                    beforeclose: function() {
                        return false;
                    }
                });
                $("#dialog")
                    .parent()
                    .find(".ui-dialog-titlebar-close")
                    .hide();

                //Manage title
                title = document.getElementById("Title");

                title.style.top =
                    config_file.Points_Panel.title.locationX + "px";
                title.style.left =
                    config_file.Points_Panel.title.locationY + "px";
                title.style.fontSize = config_file.Points_Panel.title.font_size;
                title.innerHTML = config_file.Points_Panel.title.text;
                document.getElementById("titleChangeText").value =
                    config_file.Points_Panel.title.text;

                if (config_file.Points_Panel.title.visible) {
                    title.style.border = "2px solid green";
                } else {
                    title.style.border = "2px solid red";
                }

                //Manage timer
                timer = document.getElementById("Timer");

                document.getElementById("timer_fresh_rate").value = parseInt(
                    config_file.Timer.Refresh_Rate
                );
                timer.style.top = config_file.Timer.locationX + "px";
                timer.style.left = config_file.Timer.locationY + "px";
                timer.style.fontSize = config_file.Timer.font_size;

                if (config_file.Timer.visible) {
                    timer.style.border = "2px solid green";
                } else {
                    timer.style.border = "2px solid red";
                }

                //Manage score elements
                for (
                    var i = 0;
                    i < config_file.Points_Panel.score.length;
                    i++
                ) {
                    var labels = document.getElementById("Score" + (i + 1));

                    labels.style.top =
                        config_file.Points_Panel.score[i].locationX + "px";
                    labels.style.left =
                        config_file.Points_Panel.score[i].locationY + "px";
                    console.debug(
                        labels.id +
                            " : X = " +
                            config_file.Points_Panel.score[i].locationX +
                            "px - Y = " +
                            config_file.Points_Panel.score[i].locationY +
                            "px"
                    );

                    //Font management
                    labels.style.fontSize =
                        config_file.Points_Panel.score[i].font_size;
                    labels.style.color =
                        config_file.Points_Panel.score[i].color;

                    //Background management
                    labels.style.backgroundColor =
                        config_file.Points_Panel.score[i].background_color;

                    if (config_file.Points_Panel.score[i].visible) {
                        labels.style.border = "2px solid green";
                    } else {
                        labels.style.border = "2px solid red";
                    }
                }

                displayMod(true);
            } else {
                $("#dialog")
                    .dialog()
                    .dialog("close");

                //Manage title
                title = document.getElementById("Title");

                title.style.visibility = config_file.Points_Panel.title.visible
                    ? "visible"
                    : "hidden";
                if (title.style.visibility == "visible") {
                    title.style.top =
                        config_file.Points_Panel.title.locationX + "px";
                    title.style.left =
                        config_file.Points_Panel.title.locationY + "px";
                    title.style.fontSize =
                        config_file.Points_Panel.title.font_size;
                    title.innerHTML = config_file.Points_Panel.title.text;
                }

                //Manage timer
                timer = document.getElementById("Timer");

                timer.style.visibility = config_file.Timer.visible
                    ? "visible"
                    : "hidden";

                if (timer.style.visibility == "visible") {
                    timer.style.top = config_file.Timer.locationX + "px";
                    timer.style.left = config_file.Timer.locationY + "px";
                    timer.style.fontSize = config_file.Timer.font_size;
                    timer.innerHTML = config_file.Timer.Refresh_Rate;
                }

                //Manage score elements
                for (
                    var i = 0;
                    i < config_file.Points_Panel.score.length;
                    i++
                ) {
                    var labels = document.getElementById("Score" + (i + 1));

                    labels.style.visibility = config_file.Points_Panel.score[i]
                        .visible
                        ? "visible"
                        : "hidden";

                    if (labels.style.visibility == "visible") {
                        ++listLength;

                        labels.style.top =
                            config_file.Points_Panel.score[i].locationX + "px";
                        labels.style.left =
                            config_file.Points_Panel.score[i].locationY + "px";
                        console.debug(
                            labels.id +
                                " : X = " +
                                config_file.Points_Panel.score[i].locationX +
                                "px - Y = " +
                                config_file.Points_Panel.score[i].locationY +
                                "px"
                        );

                        //Font management
                        labels.style.fontSize =
                            config_file.Points_Panel.score[i].font_size;
                        labels.style.color =
                            config_file.Points_Panel.score[i].color;

                        //Background management
                        labels.style.backgroundColor =
                            config_file.Points_Panel.score[i].background_color;
                    } else {
                        console.debug(labels.id + " : Not Shown");
                    }
                }

                displayMod(false);
            }
        }
    });
}

function findGetParameter(parameterName) {
    var result = null,
        tmp = [];
    location.search
        .substr(1)
        .split("&")
        .forEach(function(item) {
            tmp = item.split("=");
            if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
        });

    return result;
}

function displayMod(editMod) {
    if (editMod === true) {
        var labels = document.getElementsByTagName("label");
        var pointerLocation = document.getElementById("pointerLocation");

        //https://javascript.info/mouse-drag-and-drop
        for (var i = 0; i < labels.length; i++) {
            var label = labels[i];

            //Block context menu
            label.oncontextmenu = function() {
                return false;
            };

            //Manage elment moving
            label.onmousedown = function(event) {
                var current_label = this;

                if (event.button === 2) {
                    if (current_label.className != "score") {
                        if (current_label.style.borderColor == "green") {
                            current_label.style.borderColor = "red";
                        } else {
                            current_label.style.borderColor = "green";
                        }
                    } else {
                        temp_list_labels = document.getElementsByClassName(
                            "score"
                        );

                        alert(temp_list_labels.length);

                        for (var j = 0; j < temp_list_labels.length; j++) {
                            if (current_label.style.borderColor == "green") {
                                if (
                                    parseInt(
                                        temp_list_labels[j].id.replace(
                                            "Score",
                                            ""
                                        ),
                                        10
                                    ) +
                                        1 >=
                                    parseInt(
                                        current_label.id.replace("Score", ""),
                                        10
                                    ) +
                                        1
                                ) {
                                    temp_list_labels[j].style.borderColor =
                                        "red";
                                }
                            } else {
                                if (
                                    parseInt(
                                        temp_list_labels[j].id.replace(
                                            "Score",
                                            ""
                                        ),
                                        10
                                    ) +
                                        1 <=
                                    parseInt(
                                        current_label.id.replace("Score", ""),
                                        10
                                    ) +
                                        1
                                ) {
                                    temp_list_labels[j].style.borderColor =
                                        "green";
                                }
                            }
                        }
                    }
                }

                //current_label.style.position = 'absolute';
                current_label.style.zIndex = 1000;

                document.body.append(current_label);

                moveAt(event.pageX, event.pageY);

                function moveAt(pageX, pageY) {
                    current_label.style.left =
                        pageX - current_label.offsetWidth / 2 + "px";
                    current_label.style.top =
                        pageY - current_label.offsetHeight / 2 + "px";
                }

                function onMouseMove(event) {
                    moveAt(event.pageX, event.pageY);
                    pointerLocation.value =
                        "X: " + event.pageX + " - Y: " + event.pageY;
                }

                document.addEventListener("mousemove", onMouseMove);

                current_label.onmouseup = function() {
                    document.removeEventListener("mousemove", onMouseMove);
                    current_label.onmouseup = null;
                    pointerLocation.value = "";
                };

                current_label.ondragstart = function() {
                    return false;
                };
            };

            //manage element font size
            label.onwheel = function(e) {
                if (e.deltaY < 0) {
                    //Increase fontSize
                    increaseFontSizeBy(this, 1);
                }
                if (e.deltaY > 0) {
                    //Decrease fontSize
                    increaseFontSizeBy(this, -1);
                }
            };
        }
    } else {
        setInterval(updateScore, 1000);
    }
}

function increaseFontSizeBy(element, px) {
    style = window
        .getComputedStyle(element, null)
        .getPropertyValue("font-size");
    currentSize = parseFloat(style);
    element.style.fontSize = currentSize + px + "px";
}

function save() {
    //Manage title
    title = document.getElementById("Title");

    config_file.Points_Panel.title.locationX = parseInt(
        title.style.top.replace("px", "")
    );
    config_file.Points_Panel.title.locationY = parseInt(
        title.style.left.replace("px", "")
    );
    config_file.Points_Panel.title.visible =
        title.style.borderColor === "green" ? true : false;
    config_file.Points_Panel.title.font_size = title.style.fontSize;
    config_file.Points_Panel.title.text = title.innerHTML;

    //Manage timer
    timer = document.getElementById("Timer");

    config_file.Timer.Refresh_Rate = parseInt(
        document.getElementById("timer_fresh_rate").value
    );
    config_file.Timer.locationX = parseInt(timer.style.top.replace("px", ""));
    config_file.Timer.locationY = parseInt(timer.style.left.replace("px", ""));
    config_file.Timer.visible =
        timer.style.borderColor === "green" ? true : false;
    config_file.Timer.font_size = timer.style.fontSize;

    //Manage score elements
    for (var i = 0; i < config_file.Points_Panel.score.length; i++) {
        var labels = document.getElementById("Score" + (i + 1));

        //Location management
        config_file.Points_Panel.score[i].locationX = parseInt(
            labels.style.top.replace("px", "")
        );
        config_file.Points_Panel.score[i].locationY = parseInt(
            labels.style.left.replace("px", "")
        );

        //Font management
        config_file.Points_Panel.score[i].font_size = labels.style.fontSize;
        config_file.Points_Panel.score[i].color = labels.style.color;

        //Background management
        config_file.Points_Panel.score[i].background_color =
            labels.style.backgroundColor;

        //Visibility management
        config_file.Points_Panel.score[i].visible =
            labels.style.borderColor === "green" ? true : false;
    }

    //Envoi de la requête d'enregistrement
    $.ajax({
        type: "POST",
        url: "SaveScorePageConfig",
        data: { config: JSON.stringify(config_file) },
        success: function(data) {
            if (data.status == "success") {
                console.log(data.message);
                alert(data.message);
                //Tout s'est bien passé
            } else if (data.status == "error") {
                alert(data.message);
                //alert("Une erreur est survenue : \n\n " + data.message);
            }

            element.disable = false;
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            alert("Status: " + textStatus);
            alert("Error: " + errorThrown);

            element.disable = false;
        }
    });
}

function updateScore() {
    timer = document.getElementById("Timer");

    if (timer.style.visibility == "visible") {
        if (parseInt(timer.innerHTML) > 0) {
            timer.innerHTML = parseInt(timer.innerHTML) - 1;
        } else {
            //Récupération des meilleurs scores
            $.ajax({
                type: "POST",
                url: "GetTopScore",
                data: { numberOfResults: listLength },
                success: function(data) {
                    if (data.status == "success") {
                        for (var i = 0; i < data.return.scoreList.length; i++) {
                            label = document.getElementById("Score" + (i + 1));

                            label.innerHTML =
                                data.return.scoreList[i].acronym +
                                " : " +
                                data.return.scoreList[i].points;
                        }
                    } else if (data.status == "error") {
                        console.error(data.message);
                    }
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    alert("Status: " + textStatus);
                    alert("Error: " + errorThrown);

                    element.disable = false;
                }
            });

            timer.innerHTML = config_file.Timer.Refresh_Rate;
        }
    }
}
