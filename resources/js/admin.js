fill_elements();

document.getElementById('addUserButton').addEventListener('click', function() {
    addUser();
});

document.getElementById('btn_generateBDD').addEventListener('click', function() {
    alert('Fonctionnalité désactivée');
    generateBDD(this);
});

document.getElementById('editScorePageButton').addEventListener('click', function() {
    redirect('score?edit=true')
});

function redirect(link) {
    window.location.href = link;
}

var params_scoreboard;
var classes = [];
function fill_elements() {
    //var checkbox_show_timer = document.getElementById('checkbox_show_timer');

    $.ajax({
        url: "Config.json",
        cache: false,
        dataType: "json",
        success: function(data) {
            document.getElementById("checkbox_show_timer").checked =
                data.Timer.Visible;
            document.getElementById("timer_fresh_rate").value = parseInt(
                data.Timer.Refresh_Rate
            );
            document.getElementById("timer_position").value =
                data.Timer.Position;

            //console.debug(data);
        }
    });

    // $.ajax({
    //     type: "POST",
    //     url: "ListUsers",
    //     success: function(data) {
    //         if (data.status == "success") {
    //             if (typeof data.return === "undefined") {
    //                 console.log(
    //                     "Aucun utilisateur retourné, aucun affichage effectué"
    //                 );
    //             } else {
    //                 for (var j = 0; j < data.return.users.length; j++) {
    //                     var user = data.return.users[j];

    //                     // Get a reference to the table
    //                     var tableRef = document.getElementById("table_users");

    //                     // Insert a row in the table at row index 0
    //                     var newRow = tableRef.insertRow(
    //                         tableRef.rows.length - 1
    //                     );
    //                     newRow.id = "username_" + user.id;

    //                     // Insert a cell in the row at index 0
    //                     var newCell = newRow.insertCell(0);
    //                     // Append a text node to the cell
    //                     var newText = document.createTextNode(user.username);
    //                     newCell.id = "textzone_username_" + user.id;
    //                     newCell.appendChild(newText);

    //                     var newCell = newRow.insertCell(1);
    //                     newCell.innerHTML =
    //                         "<input type='checkbox' id='checkbox_admin_" +
    //                         user.id +
    //                         "' " +
    //                         (user.admin_access ? "checked" : "") +
    //                         " disabled>";

    //                     var newCell = newRow.insertCell(2);
    //                     newCell.innerHTML =
    //                         "<i class='fa fa-trash fa-lg delete_button' onClick='deleteUser(\"" +
    //                         user.id +
    //                         "\")' title='Supprimer l'utilisateur'></i><i class='fa fa-ban ban_button' aria-hidden='true' title='BAN'></i><i class='fa fa-pencil edit_button' aria-hidden='true' onClick='editUser(" +
    //                         user.id +
    //                         ")' title='Éditer'></i>";
    //                 }
    //             }
    //         } else if (data.status == "error") {
    //             alert("Une erreur est survenue : \n\n " + data.message);
    //         } else {
    //             alert("Une erreur inconnue est survenue");
    //             alert(data.message);
    //         }
    //     },
    //     error: function(XMLHttpRequest, textStatus, errorThrown) {
    //         alert("Status: " + textStatus);
    //         alert("Error: " + errorThrown);
    //     }
    // });

    $.ajax({
        url: "Classes.json",
        cache: false,
        success: function(data) {
            //console.debug(data);

            for (var i = 0; i < data.General.length; i++) {
                classes.push({
                    acronym: data.General[i].Acronym,
                    fullName: data.General[i].FullName,
                    effective: data.General[i].Effective
                });
            }

            for (var i = 0; i < data.Pro.length; i++) {
                classes.push({
                    acronym: data.Pro[i].Acronym,
                    fullName: data.Pro[i].FullName,
                    effective: data.Pro[i].Effective
                });
            }

            for (var i = 0; i < data.Agricol.length; i++) {
                classes.push({
                    acronym: data.Agricol[i].Acronym,
                    fullName: data.Agricol[i].FullName,
                    effective: data.Agricol[i].Effective
                });
            }

            document.getElementById("btn_generateBDD").disable = false;
        }
    });
}

function saveConfigFile(element) {
    element.disabled = true;
    var show_timer = document.getElementById("checkbox_show_timer").checked;
    var timer_fresh_rate = document.getElementById("timer_fresh_rate").value;
    var timer_position = document.getElementById("timer_position").value;

    $.ajax({
        type: "POST",
        url: "adminAction.php",
        data: {
            action: "updateJSON",
            Visible: show_timer,
            Refresh_Rate: timer_fresh_rate,
            Position: timer_position
        },
        success: function(data) {
            if (data.status == "success") {
                alert(data.message);
            } else if (data.status == "error") {
                alert("Une erreur est survenue : \n\n " + data.message);
            } else {
                alert("Une erreur inconnue est survenue");
                alert(data.message);
            }

            element.disabled = false;
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            alert("Status: " + textStatus);
            alert("Error: " + errorThrown);
            element.disabled = false;
        }
    });
}

function deleteUser(userID) {
    var exit = confirm("Souhaitez-vous vraiment supprimer cet utilisateur ?");
    if (exit == true) {
        //window.location = 'index.php?logout=true';

        if (userID.trim() != "") {
            $.ajax({
                type: "POST",
                url: "adminAction.php",
                data: { action: "deleteUser", user_ID: userID },
                success: function(data) {
                    //$("form#updatejob").hide(function(){$("div.success").fadeIn();});

                    if (data.status == "success") {
                        var row = document.getElementById("username_" + userID);
                        row.parentNode.removeChild(row);

                        alert(data.message);
                    } else if (data.status == "error") {
                        alert("Une erreur est survenue : \n\n " + data.message);
                    } else {
                        alert("Une erreur inconnue est survenue");
                        alert(data.message);
                    }
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    alert("Status: " + textStatus);
                    alert("Error: " + errorThrown);
                }
            });
        }
    }
}

function prevent_admin_rights(element) {
    if (element.checked == true) {
        var exit = confirm(
            "Souhaitez-vous vraiment augmenter les droits de cet utilisateur ?"
        );
        if (exit != true) {
            element.checked = false;
        }
    }
}

function addUser() {
    var username_value = document.getElementById("username");
    var password_value = document.getElementById("password");

    if (
        username_value.value.trim() != "" &&
        password_value.value.trim() != ""
    ) {
        $.ajax({
            type: "POST",
            url: "AddUser",
            data: {
                username: username_value.value,
                password: password_value.value,
                admin_access: document.getElementById("checkbox_admin").checked
                    ? true
                    : false
            },
            success: function(data) {
                //$("form#updatejob").hide(function(){$("div.success").fadeIn();});

                if (data.status == "success") {
                    // Get a reference to the table
                    var tableRef = document.getElementById("table_users");

                    // Insert a row in the table at row index 0
                    var newRow = tableRef.insertRow(tableRef.rows.length - 1);
                    newRow.id = "username_" + data.return.added_user;

                    // Insert a cell in the row at index 0
                    var newCell = newRow.insertCell(0);
                    // Append a text node to the cell
                    var newText = document.createTextNode(username_value.value);
                    newCell.id = "textzone_username_" + data.return.added_user;
                    newCell.appendChild(newText);

                    var newCell = newRow.insertCell(1);
                    newCell.innerHTML =
                        "<input type='checkbox' id='checkbox_admin_" +
                        data.return.added_user +
                        "' " +
                        (document.getElementById("checkbox_admin").checked
                            ? "checked"
                            : "") +
                        " disabled>";

                    var newCell = newRow.insertCell(2);
                    newCell.innerHTML =
                        "<i class='fa fa-trash fa-lg delete_button' onClick='deleteUser(\"" +
                        data.return.added_user +
                        "\")' title='Supprimer l'utilisateur'></i><i class='fa fa-ban ban_button' aria-hidden='true' title='BAN'></i><i class='fa fa-pencil edit_button' aria-hidden='true' onClick='editUser(" +
                        data.return.added_user +
                        ")' title='Éditer'></i>";

                    username_value.value = "";
                    password_value.value = "";
                    document.getElementById("checkbox_admin").checked = false;

                    alert(data.message);
                } else if (data.status == "error") {
                    alert("Une erreur est survenue : \n\n " + data.message);
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                alert("Status: " + textStatus);
                alert("Error: " + errorThrown);
            }
        });
    }
}

var current_user_ID = 0;
var username_original = "";
var admin_access_original = false;

function editUser(userID) {
    document.getElementById("editUser_Panel").hidden = false;

    document.getElementById(
        "checkbox_modify_user_admin"
    ).checked = document.getElementById("checkbox_admin_" + userID).checked;

    admin_access_original = document.getElementById("checkbox_admin_" + userID)
        .checked;

    document.getElementById(
        "textbox_modify_username"
    ).value = document.getElementById("textzone_username_" + userID).innerHTML;
    username_original = document.getElementById("textzone_username_" + userID)
        .innerHTML;

    current_user_ID = userID;
}

function cancelModif() {
    document.getElementById("textbox_modify_username").value = "";

    document.getElementById("checkbox_modify_user_admin").checked = false;

    document.getElementById("editUser_Panel").hidden = true;
}

var final_result = true;
var button_validate_modif;
function applyModif(element) {
    i = 0;
    button_validate_modif = element;
    button_validate_modif.disable = true;

    if (
        username_original !==
        document.getElementById("textbox_modify_username").value
    ) {
        i++;

        $.ajax({
            type: "POST",
            url: "adminAction.php",
            data: {
                action: "updateUsername",
                user_ID: current_user_ID,
                new_username: document.getElementById("textbox_modify_username")
                    .value
            },
            success: function(data) {
                //$("form#updatejob").hide(function(){$("div.success").fadeIn();});

                if (data.status == "success") {
                    console.log(data.message);
                    document.getElementById(
                        "textzone_username_" + current_user_ID
                    ).innerHTML = document.getElementById(
                        "textbox_modify_username"
                    ).value;
                    alert(
                        document.getElementById("textbox_modify_username").value
                    );
                    //Tout s'est bien passé
                } else if (data.status == "error") {
                    final_result = false;
                    //alert("Une erreur est survenue : \n\n " + data.message);
                }

                check_modif_finished();
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                alert("Status: " + textStatus);
                alert("Error: " + errorThrown);

                check_modif_finished();
            }
        });
    }

    if (
        admin_access_original !==
        document.getElementById("checkbox_modify_user_admin").checked
    ) {
        i++;

        $.ajax({
            type: "POST",
            url: "adminAction.php",
            data: {
                action: "updateAdminRight",
                user_ID: current_user_ID,
                admin_access: document.getElementById(
                    "checkbox_modify_user_admin"
                ).checked
                    ? true
                    : false
            },
            success: function(data) {
                //$("form#updatejob").hide(function(){$("div.success").fadeIn();});

                if (data.status == "success") {
                    console.log(data.message);
                    document.getElementById(
                        "checkbox_admin_" + current_user_ID
                    ).checked = document.getElementById(
                        "checkbox_modify_user_admin"
                    ).checked;
                    //Tout s'est bien passé
                } else if (data.status == "error") {
                    final_result = false;
                    //alert("Une erreur est survenue : \n\n " + data.message);
                }

                check_modif_finished();
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                alert("Status: " + textStatus);
                alert("Error: " + errorThrown);

                check_modif_finished();
            }
        });
    }
}

var i = 0;
function check_modif_finished() {
    i--;

    if (i === 0) {
        if (final_result) {
            cancelModif();
            alert("Les modifications ont été apportées avec succès");
        } else {
            alert(
                "Une problème est survenu lors de la mise à jour des informations"
            );
        }

        button_validate_modif.disable = false;
    }
}

function generateBDD(element) {
    element.disable = true;

    if (
        confirm(
            "Êtes-vous sûr de vouloir générer la base de données ? \nLes informations actuellement stockées y seront supprimées définitivement"
        )
    ) {
        $.ajax({
            type: "POST",
            url: "adminAction.php",
            data: { action: "generateBDD", Classes: JSON.stringify(classes) },
            success: function(data) {
                if (data.status == "success") {
                    console.log(data.message);
                    alert(data.message);
                    //Tout s'est bien passé
                } else if (data.status == "error") {
                    alert(data.message);
                    //alert("Une erreur est survenue : \n\n " + data.message);
                }

                element.disable = false;
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                alert("Status: " + textStatus);
                alert("Error: " + errorThrown);

                element.disable = false;
            }
        });
    } else {
        alert("génération annulée");
    }
}
