document.getElementById('updateScoreButton').addEventListener('click', function() {
    updateScore();
});

function updateScore() {
    //Récupération des meilleurs scores
    $.ajax({
        type: "POST",
        url: "GetTopScore",
        data: {
            numberOfResults: document.getElementById("number_to_show").value
        },
        success: function(data) {
            if (data.status == "success") {
                table = document
                    .getElementById("score_table")
                    .getElementsByTagName("tbody")[0];
                table.innerHTML = "";

                for (var i = 0; i < data.return.scoreList.length; i++) {
                    //label = document.getElementById("Score" + (i + 1));

                    //label.innerHTML = data.return.scoreList[i].acronym + " : " + data.return.scoreList[i].points;

                    newRow = table.insertRow(table.rows.length);
                    newCell = newRow.insertCell(0);
                    newText = document.createTextNode(table.rows.length);
                    newCell.appendChild(newText);

                    newCell = newRow.insertCell(1);
                    newText = document.createTextNode(
                        data.return.scoreList[i].acronym
                    );
                    newCell.appendChild(newText);

                    newCell = newRow.insertCell(2);
                    newText = document.createTextNode(
                        data.return.scoreList[i].points
                    );
                    newCell.appendChild(newText);
                }
            } else if (data.status == "error") {
                console.error(data.message);
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            alert("Status: " + textStatus);
            alert("Error: " + errorThrown);

            element.disable = false;
        }
    });
}
