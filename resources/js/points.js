initialize();

document.getElementById('buttonAddPoints').addEventListener('click', function() {
    addPoints();
});

function initialize() {
    $(document).keydown(function(e) {
        /*if (e.keyCode==90 && e.ctrlKey)
            {
                //$("body").append("<p>ctrl+z detected!</p>");
            }*/

        //https://www.cambiaresearch.com/articles/15/javascript-char-codes-key-codes
        switch (e.keyCode) {
            //touche "a"
            case 65:
                +5;
                break;

            //touche "q"
            case 81:
                -5;
                break;

            //touche "z"
            case 90:
                +10;
                break;

            //touche "s"
            case 83:
                -10;
                break;

            //touche "e"
            case 69:
                +20;
                break;

            //touche "d"
            case 68:
                -20;
                break;

            default:
                break;
        }
    });

    $.ajax({
        type: "POST",
        url: "ListClasses",
        success: function(data) {
            if (data.status == "success") {
                if (typeof data.return === "undefined") {
                    console.log(
                        "Aucun utilisateur retourné, aucun affichage effectué"
                    );
                } else {
                    select = document.getElementById("classes");
                    select.options.length = 0;

                    for (var j = 0; j < data.return.classes.length; j++) {
                        classe = data.return.classes[j];

                        select.options[select.options.length] = new Option(
                            classe.acronym + " - " + classe.fullName,
                            classe.acronym
                        );
                    }

                    $(".chosen").chosen();
                }
            } else if (data.status == "error") {
                alert("Une erreur est survenue : \n\n " + data.message);
            } else {
                alert("Une erreur inconnue est survenue");
                alert(data.message);
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            alert("Status: " + textStatus);
            alert("Error: " + errorThrown);
        }
    });
}

function isInt(value) {
    return (
        !isNaN(value) &&
        parseInt(Number(value)) == value &&
        !isNaN(parseInt(value, 10))
    );
}

function addPoints() {
    classes = document.getElementById("classes");
    points = document.getElementById("points");
    log = document.getElementById("log-console");

    if (!isInt(points.value)) {
        return false;
    } else {
        $.ajax({
            type: "POST",
            url: "AddPoints",
            data: {
                classe: classes.value,
                points: points.value
            },
            success: function(data) {
                if (data.status == "success") {
                    //alert("Points ajoutés avec succès");

                    log.append(
                        points.value +
                            " points ajoutés à " +
                            classes.value +
                            "\n"
                    );

                    points.value = 0;
                } else if (data.status == "error") {
                    alert("Une erreur est survenue : \n\n " + data.message);
                } else {
                    alert("Une erreur inconnue est survenue");
                    alert(data.message);
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                alert("Status: " + textStatus);
                alert("Error: " + errorThrown);
            }
        });
    }
}
