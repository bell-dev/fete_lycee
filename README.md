<p align="center"><img src="https://institut-lemonnier.fr/wp-content/themes/lemonnier/assets/images/logo.svg"></p>
<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

# Gestion des points : fête du lycée Institut Lemonnier

## Environnement
- Laravel 5.8
- MySQL
- PHP >= 7.1.3
- OpenSSL PHP Extension
- PDO PHP Extension
- *NodeJS (Développement uniquement)*

## Première installation du projet
- `$ git clone`
- Se rendre dans le dossier du projet, à la racine
- `$ php composer.phar install [--no-dev]` paramètres en cas de déploiement en production
- `$ cp .env.example .env`
- Configuration du fichier .env pour pointer vers la base de données
- `$ php artisan migrate --seed` base de donnée nommée `fete_lycee`
- Lancer le site en allant dans `public/`
- *`$ npm insall` (Développement uniquement)*

Utilisateur ar défaut :
- login: `admin@institut-lemonnier.fr`
- mdp: `Password1234`

### Pages
- `/score` Affichage du score
- `/admin` Page d'administration
- `/home` Rentrée des points


## Édition du projet
Modifier le CSS et JS présent dans `resources/` si nécessaire, puis exécuter la commande `$ npm run prod`, ou `$ npm run watch` pour mise à jour en direct
